<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entradas".
 *
 * @property int $id
 * @property string $titulo
 * @property string $texto
 * @property string $fecha
 */
class Entradas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'titulo', 'texto', 'fecha'], 'required'],
            [['id'], 'integer'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 200],
            [['texto'], 'string', 'max' => 800],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Codigo de entrada',
            'titulo' => 'Titulo',
            'texto' => 'Descripcion',
            'fecha' => 'Fecha de entrada',
        ];
    }
}
